json.array!(@students) do |student|
  json.extract! student, :id, :Name, :Gender, :Weight, :Height, :Colour, :GPA, :Campus, :Faculty
  json.url student_url(student, format: :json)
end
