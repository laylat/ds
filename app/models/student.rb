class Student < ActiveRecord::Base
    validates :Name, presence: true, allow_nil: false,  format: { with: /\A[a-zA-Z]+\z/, message: "field only allows letters" }
    validates :Weight, presence: true, allow_nil: false, numericality: {greater_than: 15, less_than: 600, message: "value must be reasonable for a person"  }
    validates :Height, presence: true, allow_nil: false,  numericality: {greater_than: 30, less_than: 300, message: "value must be reasonable for a person"  }
    validates :GPA, presence: true, allow_nil: false, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 4.33, message: "for SFU is between 0 and 4.33"  }
    
end
