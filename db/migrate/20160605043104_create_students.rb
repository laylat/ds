class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.text :Name
      t.text :Gender
      t.float :Weight
      t.float :Height
      t.text :Colour
      t.float :GPA
      t.text :Location
      t.text :Faculty

      t.timestamps null: false
    end
  end
end
