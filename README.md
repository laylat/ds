== README
* heroku URL: https://thawing-thicket-23931.herokuapp.com/
* bitbucket URL: https://bitbucket.org/laylat/ds
* heroku git link: https://git.heroku.com/thawing-thicket-23931.git

* Notes:
* MVC successfully created WITHOUT a scaffold

* Features:
* 1. GENDER: added a gender parameter
* 3 options: Female, Male, Other (to be politically correct - nice touch!)
* the border of the students displayed is coloured according to gender: (male=blue, female=pink, other=yellow)
* *
* 2. Included form validation
* 
* 3. Students are filled with their respective colours
* 
* 4. MULTIPLE DISPLAYS: 
* User can toggle 5 ways, click to display...
* Included display by body mass index, campus, faculty and deans list
* 
* 5. Learn features button:
* *Alert box that says some of what I wrote in this file
